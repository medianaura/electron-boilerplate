const prompts = require('@vigilance/automaton-config-library/lib/prompts/choose-semver');
const zip = require('@vigilance/automaton-config-library/lib/common/archive.zip');
const { Version } = require('@vigilance/automaton-config-library/lib/common/semver');
const AdmZip = require('adm-zip');
const child_process = require('child_process');
const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

const appName = 'electron-boilerplate';
const deployDirectory = 'S:\\Programmation\\RxvApps\\' + appName;
const version = new Version();
const files = fs.readdirSync(deployDirectory).sort().reverse().slice(0, 10);

module.exports = {
  jobs: [
    {
      id: 'version',
      name: 'Create a version',
      prompts: prompts(version),
      actions: [
        {
          run: (answers) => {
            if (!answers.confirm) {
              process.exit(4);
            }
          },
        },
        {
          cmd: 'vc generate --no-commit --next %(version)s',
        },
        {
          cmd: 'npm version --force --no-git-tag-version %(version)s',
        },
        {
          cmd: 'git add .',
        },
        {
          cmd: 'git commit -m "doc: mise à jour du changelog pour la version %(version)s"',
        },
        {
          cmd: 'git tag -a v%(version)s -m "doc: creation de la version %(version)s"',
        },
        {
          cmd: 'git push',
        },
        {
          cmd: 'git push --tags --no-verify',
        },
        {
          cmd: 'automaton package',
        },
        {
          cmd: 'rimraf .dist',
        },
        {
          cmd: 'npm run release:package',
        },
        {
          run: (answer) => {
            rimraf.sync('./.dist/win-unpacked');
            fs.unlinkSync('./.dist/builder-debug.yml');
            fs.unlinkSync('./.dist/builder-effective-config.yaml');
            zip('.dist', deployDirectory, appName + '-' + answer.version + '.zip');
          },
        },
      ],
    },
    {
      id: 'package',
      name: 'Create an application package',
      prompts: [],
      actions: [
        {
          cmd: 'npm run build',
        },
      ],
    },
    {
      id: 'deploy',
      name: 'Deploy a zip',
      prompts: [
        {
          type: 'confirm',
          name: 'deployZip',
          message: 'Voulez vous deployé une version :',
        },
        {
          type: 'list',
          name: 'version',
          message: `Quel version veut-on déployé :`,
          choices: files,
          when: function (answers) {
            return answers.deployZip;
          },
        },
        {
          type: 'list',
          name: 'deployTo',
          message: `Déployé sur :`,
          choices: ['U', 'M'],
        },
      ],
      actions: [
        {
          run: (answers) => {
            const directoryRelease = path.resolve(answers.deployTo + ':', '\\_Programmes\\Utilitaires\\' + appName + '\\release');
            const zipFile = path.resolve(deployDirectory, answers.version);

            rimraf.sync(directoryRelease);
            fs.mkdirSync(directoryRelease);

            const admZip = new AdmZip(zipFile, {});
            admZip.extractAllTo(directoryRelease, true, '');
          },
          when: (answers) => {
            return answers.deployZip;
          },
        },
        {
          run: (answers) => {
            const diskSource = answers.deployTo === 'U' ? 'C' : 'U';
            const diskDestination = answers.deployTo;

            const directoryDestination = path.resolve(diskDestination + ':', '\\_Programmes\\Utilitaires\\' + appName);
            const directorySource = path.resolve(diskSource + ':', '\\_Programmes\\Utilitaires\\' + appName);

            child_process.spawnSync('C:\\Program Files (x86)\\Beyond Compare 3\\BCompare.exe', [directoryDestination, directorySource]);
          },
        },
      ],
    },
  ],
};
