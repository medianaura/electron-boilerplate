# Checklist de code review pour Web

**Avant le code-review: N'oubliez-pas de lire [les bonnes pratiques de code-review](https://git.vigilance.local/code-review/code-review-standards-web/blob/develop/GUIDELINES.md)**

Voir le [Changelog.md](CHANGELOG.md) pour les modifications
#### Description des changements
---

[//]: # (Inscrire ici les détails des changements apportés. Donnez du contexte et ajoutez des liens vers la doc.)
Travail pour la tache [DEV-AAAA](https://jira.vigilance.local/browse/DEV-AAAA)

---

## Développeur
- [ ] Les commits utilise les conventions de commit vigilance.
- [ ] La branche est à jour a partir de la branche approprié.
- [ ] Relire les modifications apportées

#### Tests
- [ ] Vérification localement des changements apportés
- [ ] Résultat des outils de CI locaux positif

## Réviseur
- [ ] Les messages de commits sont clairs et descriptifs.
- [ ] Au moins un commit contient le flag log avec un message de body.
- [ ] Est lisible, facile à comprendre et les noms sont significatifs.
- [ ] La language utilisé suit [les standards](https://git.vigilance.local/technologie/models_and_standards/wikis/Langue-dans-le-d%C3%A9veloppement).
- [ ] Répond au besoin, pas de changements "tant qu'à y être".
- [ ] Répond au Standard/Best-practice du langage de programmation (Tab, Space, Bracket, etc..)
- [ ] Répond au critères d'acceptation technique



@code\-review @cpp\-code\-review

[//]: # (Template version: 1.3.0)
