// https://docs.cypress.io/guides/guides/plugins-guide.html

// if you need a custom webpack configuration you can uncomment the following import
// and then use the `file:preprocessor` event
// as explained in the cypress docs
// https://docs.cypress.io/api/plugins/preprocessors-api.html#Examples

const { initPlugin } = require('cypress-plugin-snapshots/plugin');
const browserify = require('@cypress/browserify-preprocessor');
const cucumber = require('cypress-cucumber-preprocessor').default;
const resolve = require('resolve');

module.exports = (on: any, config: any) => {
  const options = {
    ...browserify.defaultOptions,
    typescript: resolve.sync('typescript', { baseDir: config.projectRoot }),
  };

  initPlugin(on, config);
  on('file:preprocessor', cucumber(options));

  return Object.assign({}, options, {
    fixturesFolder: 'tests/e2e/fixtures',
    testFiles: '**/*.{feature,features}',
    integrationFolder: 'tests/e2e/integration',
    screenshotsFolder: 'tests/e2e/screenshots',
    videosFolder: 'tests/e2e/videos',
    supportFile: 'tests/e2e/support/index.ts',
  });
};
