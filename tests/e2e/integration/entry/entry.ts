import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given('I open Google page', () => {
  cy.visit('http://www.google.ca');
});

Then('I see {string} on the screen', (text: string) => {
  cy.get('body > div.L3eUgb > div.o3j99.n1xJcf.Ne6nSd > a:nth-child(2)').contains(text);
});
