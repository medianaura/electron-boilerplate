import path from 'path';

jest.mock('path');

jest.spyOn(path, 'resolve').mockImplementation((...args) => args.join('\\').replace(/\\\\/gm, '\\'));
jest.spyOn(path, 'basename').mockImplementation((s) => s);
