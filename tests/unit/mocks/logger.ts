const Logger = {
  emit: jest.fn(),
  setEmiter: jest.fn(),
};

jest.mock('@root/src/common/services/logger', () => {
  return {
    Logger: Logger,
  };
});
