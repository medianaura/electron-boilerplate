import fs from 'fs';

jest.mock('fs');

const mock_writeFileSync = jest.spyOn(fs, 'writeFileSync').mockImplementation();
const mock_readFileSync = jest.spyOn(fs, 'readFileSync').mockImplementation();
const mock_unlinkSync = jest.spyOn(fs, 'unlinkSync').mockImplementation();

export { mock_readFileSync, mock_writeFileSync, mock_unlinkSync };
