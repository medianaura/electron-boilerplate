# Electron Boilerplate

## Installation

Pour l'installation :

1. Faire un clone du projet
1. Supprimé le dossier .git
    1. Lancer la commande ```git init```
    1. Appliquer le nouveau repo ``git remote add origin URL_REPO``
1. Modifier le fichier ``src/electron/installer.nsh`` pour créer les bons raccourcis
1. Modifier le fichier ``CHANGELOG.md`` pour avoir le bon nom d'application
1. Modifier la configuration dans ``package.json`` :

````json
{
    "name": "electron-boilerplate",
    "version": "1.0.0",
    "description": "",
    "license": "ISC",
    "author": "Sébastien Lafleur",
    "build": {
        "appId": "ca.vigilance.electron",
        "productName": "Electron Boilerplate",
        "publish": {
            "url": "http://mdrive.vigilance.local/_programmes/utilitaires/electron/release/"
        }
    },
    "config": {
        "title": "Electron Boilerplate",
        "electron": {
            "width": 1230,
            "height": 850,
            "resizable": false
        }
    }
}
````

## Developpement

1. Visuel de l'application
    1. Lancer la commande : ``npm run serve:frontend``

1. Backend de l'application
    1. Lancer les commandes : ``npm run watch:backend`` et ``npm run serve:backend``

## Verification

Lancer la commande : ``npm run ci``

Vérification effectué : TypeScript Type checking, Linting (Style, Script et Vue) et Prettier

## Test

### Test Unitaire

Lancer la commande : ``npm run test:unit``
Si on veut watch : ``npm run test:unit:watch``

### Test E2E

Lancer la commande : ``npm run test:e2e:open``
Si on veut en ligne de command : ``npm run test:e2e:run``

## Release

### Creation d'une version locale (Pour des tests)

Lancer la commande : ``npm run release:dev``

### Creation d'une version

Lancer la commande : ``npm run release``

Suivre les étapes a l'écran. Déplacé les fichiers produits dans le dossier .dist a l'endroit approprié pour la MAJ automatique ou les tests usagés.

## Structure

### Dossier 

**backend** : Le dossier backend contient la logique métier de l'application qui ne conserne pas le visuel de l'application.

**common** : Le dossier common contient les classes communes de l'application. Le code source qui sera utilisé dans les workers, backend et frontend de l'application.

**electron** : Le dossier electron contient la logique pour créer le container Electron, les shortcuts d'installation et la logique de mise a jour de l'application. Le dossier n'aura pas besoin de modification en général.

**frontend** : Le dossier frontend contient la logique métier de l'application pour l'experience utilisateur.

**thread** : Le dossier thread contient les logiques pour créer les workers de l'application. Le dossier n'aura pas besoin de modification en général.

**worker** : Le dossier worker contient la logique métier de l'application pour les jobs extensives qui doivent être executés par un autre processus.
