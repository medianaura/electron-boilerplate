import { fork } from 'child_process';
import { CliInformation } from '@backend/interfaces/cli-information';
import { getScriptPath } from '@common/helpers/utility';

export async function setupServer(info: CliInformation): Promise<CliInformation> {
  return new Promise((resolve) => {
    void startServer(info).then((info) => {
      resolve(info);
    });
  });
}

async function startServer(info: CliInformation): Promise<CliInformation> {
  const forkProcessPath = getScriptPath('server.js', info.path);
  let _resolve: any;

  const promise = new Promise<CliInformation>((resolve) => {
    _resolve = resolve;
  });

  const server = fork(forkProcessPath, [], {
    stdio: ['inherit', 'inherit', 'inherit', 'ipc'],
  });

  void server.on('message', (message) => {
    if (message === 'started_server') {
      return _resolve(info);
    }

    console.log('message');
  });

  server.send({ message: 'information', data: info });
  return promise;
}
