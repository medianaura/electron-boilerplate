const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const path = require('path');
const url = require('url');
const debugFeature = require('electron-debug');
const p = require('@root/package.json');

export class BaseApplication {
  private win: any;
  private splash: any;

  constructor() {
    app.on('window-all-closed', () => {
      if (process.platform !== 'darwin') {
        app.quit();
      }
    });
  }

  public createWindow(debug: boolean, web: boolean): void {
    app.whenReady().then(() => {
      const config = Object.assign(
        {
          width: 1400,
          height: 900,
          title: p.config.title,
          show: false,
          titleBarStyle: 'hidden',
          icon: 'M:\\RxvTravail\\Images\\icones\\vigilance-sante.ico',
          webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
          },
        },
        p.config.electron,
      );
      // Create the browser window.
      this.win = new BrowserWindow(config);

      let urlPrecursor = {
        pathname: path.resolve(__dirname, '../frontend/', 'index.html'),
        protocol: 'file:',
        slashes: true,
      };

      if (web) {
        urlPrecursor = {
          pathname: 'localhost:3000',
          protocol: 'http:',
          slashes: true,
        };
      }

      // And load the index.html of the app.
      this.win.loadURL(url.format(urlPrecursor));

      ipcMain.handle('showOpenDialogSync', async (event: any, options: any) => {
        return dialog.showOpenDialogSync(options);
      });

      this.win.once('ready-to-show', () => {
        if (debug) {
          debugFeature();
          this.win.webContents.openDevTools({ mode: 'detach' });
        }

        // TODO : Activate this to close splash screen
        // this.splash.close();
        this.win.show();
      });

      this.win.on('closed', () => {
        process.exit(0); // eslint-disable-line unicorn/no-process-exit
      });
    });
  }

  public createSplashScreen(): void {
    this.splash = new BrowserWindow({
      width: 450,
      height: 650,
      transparent: true,
      frame: false,
      alwaysOnTop: true,
    });

    this.splash.loadURL(
      url.format({
        pathname: path.resolve(__dirname, '../frontend/', 'splash.html'),
        protocol: 'file:',
        slashes: true,
      }),
    );
  }
}
