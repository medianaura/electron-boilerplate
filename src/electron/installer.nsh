!macro customInstall
    ${ifNot} ${isUpdated}
        CreateShortCut "$desktop\electron-boilerplate.lnk" "$INSTDIR\electron-boilerplate.exe"
    ${endIf}
!macroend

!macro customUnInstall
    ${ifNot} ${isUpdated}
        Delete "$desktop\electron-boilerplate.lnk"
    ${endIf}
!macroend
