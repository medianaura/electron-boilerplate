export interface CliInformation {
  level: string;
  local: boolean;
  web: boolean;
  debug: boolean;
  port: number;
  env: string;
  path: string;
  file?: string;
}
