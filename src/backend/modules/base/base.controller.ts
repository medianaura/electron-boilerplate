import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { InjectFromContainer } from 'di-manager';
import { ConfigurationService } from '@common/services/configuration';
import { Socket } from 'socket.io';

@WebSocketGateway()
export class BaseController {
  @InjectFromContainer('ConfigurationService')
  private readonly configurationService!: ConfigurationService;

  @SubscribeMessage('initialisation')
  public async init(): Promise<any> {
    this.configurationService.setInformation();

    return {
      configuration: this.configurationService.configuration,
    };
  }

  @SubscribeMessage('change_environnement')
  public async changeEnvironnement(@MessageBody() data: string, @ConnectedSocket() client: Socket): Promise<any> {
    this.configurationService.setValue('env', data);
    client.emit('initialisation', await this.init());
  }
}
