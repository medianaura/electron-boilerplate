import { Module } from '@nestjs/common';
import { BaseController } from '@backend/modules/base/base.controller';

@Module({
  providers: [BaseController],
})
export class BaseModule {}
