import { PoolHelper } from '@src/thread/pool-helper';
import { Job } from '@src/thread/job';

export class PoolExemple {
  constructor() {
    PoolHelper.getInstance().createPool();

    // Send configuration to initially setup worker
    PoolHelper.getInstance().setupWorker();

    // Sent when all the worker are ready
    PoolHelper.getInstance().bindSetup(this.setup);

    // Sent when a setup fail (Manully declared in the worker
    PoolHelper.getInstance().bindSetupError(this.setupError);

    // Manully declared in the worker
    PoolHelper.getInstance().bindProgress(this.progress);

    // Manully declared in the worker (When a job is done to queue the next one)
    PoolHelper.getInstance().bindDone(this.done);

    // Sent when all the job are finished
    PoolHelper.getInstance().bindFinished(this.finished);
  }

  public init(): void {
    PoolHelper.getInstance().addJob(new Job({ cmd: 'env', options: { bob: 1 } }));
  }

  public setup(m: any): void {
    console.log(m);
  }

  public setupError(m: any): void {
    console.log(m);
  }

  public progress(m: any): void {
    console.log(m);
  }

  public done(m: any): void {
    console.log(m);
  }

  public finished(): void {
    console.log('finished');
  }
}
