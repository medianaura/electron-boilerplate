import 'reflect-metadata';
import 'core-js';

import { container } from 'tsyringe';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SocketAdapter } from '@backend/adapters/socket-adapter';

require('dotenv-flow').config();

async function bootstrap(port: number) {
  const app = await NestFactory.create(AppModule, { cors: true });

  app.useWebSocketAdapter(new SocketAdapter(app));

  await app.listen(port, '127.0.0.1');
  console.log(`Running on ${await app.getUrl()}`);
}

process.on('message', async (message: any) => {
  if (message.message !== 'information') {
    return;
  }

  container.register('info', { useValue: message.data });
  await bootstrap(message.data.port);

  process.send('started_server');
});
