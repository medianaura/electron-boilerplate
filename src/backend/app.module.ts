import { Module } from '@nestjs/common';
import { BaseModule } from '@backend/modules/base/base.module';
import { container } from 'tsyringe';
import { CliInformation } from '@backend/interfaces/cli-information';
import { LoggerService } from '@common/services/logger';
import { InjectFromContainer } from 'di-manager';
import { ConfigurationService } from '@common/services/configuration';

@Module({
  imports: [BaseModule],
})
export class AppModule {
  @InjectFromContainer('ConfigurationService')
  private readonly configurationService!: ConfigurationService;

  @InjectFromContainer('LoggerService')
  private readonly loggerService!: LoggerService;

  constructor() {
    const info = container.resolve<CliInformation>('info');
    this.loggerService.setLevel(info.level);
    this.configurationService.setInformation();
  }
}
