import Vue, { VNode } from 'vue';

declare global {
  namespace JSX {
    interface Element extends VNode {}
    interface ElementClass extends Vue {}
    // eslint-disable-next-line @typescript-eslint/consistent-indexed-object-style
    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
}
