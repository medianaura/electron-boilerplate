import 'reflect-metadata';
import 'core-js';

// Fix for Caporal to add an argv at position 1 ... BUT WHY !!!!
if (process.argv.length === 1) {
  process.argv.push('');
}

import { program } from '@caporal/core';
import { isElectron } from 'environ';
import packages from '../package.json';
import { setStarterSetting } from '@common/helpers/starter-setting';
import { CliInformation } from '@backend/interfaces/cli-information';
import { BaseUpdater } from '@src/electron/update';
import { setupServer } from '@src/electron/server';
import path from 'path';

let settings: CliInformation;

program
  .name(packages.name)
  .version(packages.version)
  .strict(false)
  .action(async () => {
    settings.path = __dirname;
    settings.env = 'PROD';

    // Do not update when we are in local mode.
    if (!settings.local) {
      const update: any = await new BaseUpdater().checkForUpdate();
      const downloadPromise = update.downloadPromise;
      if (typeof downloadPromise !== 'undefined') {
        return;
      }
    }

    const { choosePortSync } = require('choose-port');
    settings.port = choosePortSync(settings.port, '127.0.0.1');

    if (isElectron()) {
      const { BaseApplication } = require('@src/electron');
      const baseApp = new BaseApplication();

      settings = await setupServer(settings);

      baseApp.createWindow(settings.debug, settings.web);
      (global as any).port = settings.port;
    } else {
      settings = await setupServer(settings);
    }
  });

(async () => {
  settings = setStarterSetting();
  const args = ['electron.exe', 'node.exe'].includes(path.basename(process.argv[0])) ? process.argv.slice(2) : process.argv.slice(1);
  await program.run(args);
})();
