const { ipcRenderer, contextBridge } = require('electron');

contextBridge.exposeInMainWorld('electron', {
  dialog: {
    showOpenDialogSync: (options) => ipcRenderer.invoke('showOpenDialogSync', options),
  },
});
