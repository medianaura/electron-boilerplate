import Vue from 'vue';
import VueRx from 'vue-rx';
import VueSocketIOExt from 'vue-socket.io-extended';
import { io } from 'socket.io-client';

// <---- Configuration
const p = require('../../package.json');
let port = p.config.port;
if (typeof window.require !== 'undefined') {
  const remote = window.require('electron').remote;
  port = remote.getGlobal('port');
}

const socket = io(`http://127.0.0.1:${port}`);

// <---- INSERT IMPORT HERE

Vue.use(VueRx);
Vue.use(VueSocketIOExt, socket);
