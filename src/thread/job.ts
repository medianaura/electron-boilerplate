import { Worker } from 'cluster';
import { EventEmitter } from 'eventemitter3';
import { JobInformation } from '@src/thread/interfaces/job-information';

export class Job extends EventEmitter {
  public options: JobInformation;

  constructor(options: JobInformation) {
    super();
    this.options = options;
  }

  workOn(worker: Worker): void {
    worker.send(this.options);
  }
}
