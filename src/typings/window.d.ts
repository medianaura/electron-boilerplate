declare interface Window {
  electron: {
    dialog: {
      showOpenDialogSync: (options: any) => Promise<string[]>;
    };
  };
}

declare module 'vue-socket.io';
declare module 'vue-loading-overlay';
