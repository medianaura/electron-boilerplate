import parser from 'yargs-parser';
import { CliInformation } from '@backend/interfaces/cli-information';

const p = require('../../../package.json');

export function setStarterSetting(): CliInformation {
  let level = '0';

  // Activate verbose mode
  const argv = parser(process.argv);

  if (argv.v || argv.verbose) {
    level = '4';
  }

  let local = false;
  if (argv.local) {
    local = argv.local;
  }

  let web = false;
  if (argv.web) {
    web = argv.web;
  }

  let debug = false;
  if (argv.debug) {
    debug = argv.debug;
    level = '5';
  }

  let port = p.config.port;
  if (argv.port) {
    port = argv.port;
  }

  return {
    level,
    local,
    web,
    debug,
    port,
    env: '',
    path: '',
  };
}
