import { classToPlain, plainToClass } from 'class-transformer';
import { DiskInformation } from '@common/interfaces/disk-information';

export class Configuration {
  public disk: DiskInformation = { config: '', data: '', stockage: '' };

  public env!: string;
  public version!: string;

  public configPath = '%(disk_config)s\\_programmes\\Utilitaires\\electron-boilerplate\\Config\\';
  public tempPath = 'c:\\bidon\\electron-boilerplate\\';

  public static fromJSON(payload: Record<string, unknown>): Configuration {
    return plainToClass(Configuration, payload);
  }

  public toJSON(): Record<string, unknown> {
    return classToPlain(this);
  }
}
