import { Expose, Transform } from 'class-transformer';
import { convertMapToPlain } from '@common/helpers/utility';

export class LocaleInformation {
  @Expose()
  public default = 'fr';

  @Expose()
  @Transform((p) => new Map(p.value ? Object.entries(p.value) : Object.entries({})), { toClassOnly: true })
  @Transform((p) => convertMapToPlain(p.value), { toPlainOnly: true })
  public output!: Map<string, string>;
}
