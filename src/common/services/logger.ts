import { Socket } from 'socket.io';
import { registry } from 'tsyringe';
import { createLogger, transports, Logger, format } from 'winston';
const { combine, timestamp, prettyPrint, ms } = format;

export enum LOGGER_LEVELS {
  'error' = 'error',
  'warn' = 'warn',
  'info' = 'info',
  'http' = 'http',
  'verbose' = 'verbose',
  'debug' = 'debug',
  'silly' = 'silly',
}

@registry([{ token: 'LoggerService', useValue: new LoggerService() }])
export class LoggerService {
  /* eslint-disable quote-props */
  public static CODES: Record<string, string> = {
    '0': 'error',
    '1': 'warn',
    '2': 'info',
    '3': 'http',
    '4': 'verbose',
    '5': 'debug',
    '6': 'silly',
  };
  /* eslint-enable quote-props */

  private emitter: Socket = null;

  private readonly logger: Logger = null;

  constructor() {
    this.logger = createLogger({
      format: combine(timestamp({ format: 'YYYY-MM-DDTHH:mm:ssZZ-0500' }), ms(), prettyPrint()),
      transports: [new transports.Console()],
    });
  }

  public setLevel(level: string): void {
    this.logger.transports[0].level = LoggerService.CODES[level];
  }

  public emit(message: any, level: LOGGER_LEVELS = LOGGER_LEVELS.info, event?: string): void {
    if (this.emitter && event) {
      this.sendMessage(message, event);
    }

    this.log(message, level);
  }

  public log(message: any, level: LOGGER_LEVELS): void {
    if (!level) return;
    this.logger.log(level, message);
  }

  public setEmitter(emitter: Socket): void {
    this.emitter = emitter;
  }

  private sendMessage(message: any, event: string): void {
    this.emitter.emit(event, message);
  }
}
