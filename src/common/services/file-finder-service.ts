import FindFiles from 'node-find-files';

export class FileFinderService {
  public static PROGRAMFILES: string = process.env.PROGRAMFILES;
  public static PROGRAMFILESX86: string = process.env['PROGRAMFILES(X86)'];

  private static instance: FileFinderService;

  public static getInstance(): FileFinderService {
    if (typeof this.instance === 'undefined') {
      this.instance = new FileFinderService();
    }

    return this.instance;
  }

  public async setFinder(rootDirectories: string[], filterFunction: any): Promise<string[]> {
    return new Promise((resolve) => {
      void Promise.all(rootDirectories.map(async (rootDirectory) => this.runFinder(rootDirectory, filterFunction))).then((values) => {
        resolve(values.reduce((accumulator, value) => accumulator.concat(value), []));
      });
    });
  }

  private async runFinder(rootDirectory: string, filterFunction: any): Promise<string[]> {
    return new Promise((resolve) => {
      const files: string[] = [];

      const finder = new FindFiles({
        rootFolder: rootDirectory,
        filterFunction,
      });

      finder.on('match', (filePath) => {
        files.push(filePath);
      });

      finder.on('complete', () => {
        resolve(files);
      });

      finder.startSearch();
    });
  }
}
