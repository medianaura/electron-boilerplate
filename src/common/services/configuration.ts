import packageJSON from '@root/package.json';
import Conf from 'conf';
import { registry } from 'tsyringe';
import { InjectFromContainer } from 'di-manager';
import { Configuration } from '@common/models/configuration';
import { PathUtility } from '@common/services/path-utility';
import { getDriveFromEnvironment } from '@common/helpers/utility';

@registry([{ token: 'ConfigurationService', useValue: new ConfigurationService() }])
export class ConfigurationService {
  @InjectFromContainer('PathUtility')
  public pathUtility!: PathUtility;

  public configuration!: Configuration;

  private readonly userConfig!: Conf;

  constructor() {
    this.userConfig = new Conf();
    this.configuration = new Configuration();
  }

  public getUserConfig(): Conf {
    return this.userConfig;
  }

  public getValue<T>(key: string, value: T): T {
    if (this.userConfig.has(key)) {
      return this.userConfig.get(key) as T;
    }

    return value;
  }

  public setValue<T>(key: string, value: T): void {
    this.userConfig.set(key, value);
  }

  public setInformation(): void {
    const env = this.getValue('env', 'PROD');

    this.configuration = Configuration.fromJSON({
      env,
      disk: getDriveFromEnvironment(env),
      version: packageJSON.version,
    });

    this.pathUtility.setCurrentDrive(this.configuration.disk);
    this.configuration.configPath = this.pathUtility.getPath(this.configuration.configPath);
  }
}
