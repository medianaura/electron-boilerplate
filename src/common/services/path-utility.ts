import fs from 'fs';
import { resolve } from 'path';
import { set } from 'lodash';
import { registry } from 'tsyringe';
import { sprintf } from 'sprintf-js';
import { DiskInformation } from '@common/interfaces/disk-information';
import { LocaleInformation } from '@common/models/locale-information';

@registry([{ token: 'PathUtility', useValue: new PathUtility() }])
export class PathUtility {
  private replacements: any = {};

  public setCurrentDrive(drive: DiskInformation): void {
    this.replacements.disk = drive.data;
    this.replacements.disk_s = drive.stockage;
    this.replacements.disk_config = drive.config;
  }

  public setReplacementInformation(info: LocaleInformation): void {
    this.replacements.locale = info.default;

    if (!info.output) {
      return;
    }

    info.output.forEach((value, key) => {
      set(this.replacements, ['output', key], value);
    });
  }

  public getPath(path: string, info?: LocaleInformation): string {
    if (typeof info !== 'undefined') {
      this.setReplacementInformation(info);
    }

    return resolve(sprintf(path, this.replacements));
  }

  public getJSON(path: string, info?: LocaleInformation): any {
    return JSON.parse(this.getData(path, 'utf8', info));
  }

  public getData(path: string, encoding: BufferEncoding = 'utf8', info?: LocaleInformation): string {
    return fs.readFileSync(this.getPath(path, info), { encoding });
  }
}
