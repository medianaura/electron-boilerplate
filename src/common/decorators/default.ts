import { Transform } from 'class-transformer';

export function Default(defaultValue: any): any {
  return Transform((p) => (p.value !== null && p.value !== undefined ? p.value : defaultValue), { toClassOnly: true });
}
