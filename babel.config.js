module.exports = (api) => {
  if (api.env() === 'test') {
    return {
      presets: [['@babel/env']],
    };
  }

  return {
    presets: [['@babel/preset-env', { modules: false, useBuiltIns: 'usage', corejs: '3' }]],
  };
};
