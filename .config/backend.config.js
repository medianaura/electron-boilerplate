const path = require('path');
const webpack = require('webpack');
const DotenvFlow = require('dotenv-flow-webpack');
// const CopyPlugin = require('copy-webpack-plugin');
const config = require('./default.config');

const c = {
  entry: {
    main: path.resolve(__dirname, '../src/entry.ts'),
    preload: path.resolve(__dirname, '../src/preload.js'),
    server: path.resolve(__dirname, '../src/backend/main.ts'),
    worker: path.resolve(__dirname, '../src/worker/main.ts'),
  },
  output: {
    path: path.resolve(__dirname, '../.out/backend'),
    filename: '[name].js',
  },
};

module.exports = (env, argv) => {
  Object.assign(config, c);

  config.plugins.push(new DotenvFlow());

  config.optimization.minimize = argv.mode === 'production';

  return config;
};
