const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const config = require('./default.config');
const p = require('../package.json');
const { choosePortSync } = require('choose-port');

const port = choosePortSync(3000, '127.0.0.1');

const c = {
  target: 'web',
  entry: path.resolve(__dirname, '../src/frontend/main.ts'),
  output: {
    path: path.resolve(__dirname, '../.out/frontend'),
    publicPath: '/',
    filename: '[name].js',
  },
  externals: [],
  devServer: {
    open: true,
    hot: true,
    historyApiFallback: true,
    compress: false,
    port: port,
  },
};

module.exports = (env, argv) => {
  Object.assign(config, c);

  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(argv.mode === 'production' ? 'production' : 'development'),
        BASE_URL: '"./"',
      },
    }),
  );

  config.plugins.push(
    new HtmlWebpackPlugin({
      template: './pages/index.html',
      templateParameters: {
        BASE_URL: './',
        TITLE: p.config.title,
      },
      filename: 'index.html',
    }),
  );

  config.plugins.push(
    new CopyPlugin({
      patterns: [
        {
          from: path.join(__dirname, '../', 'public'),
          to: path.join(__dirname, '../', '.out'),
          toType: 'dir',
        },
      ],
    }),
  );

  if (argv.mode === 'production') {
    config.output.publicPath = './';
  }
  config.optimization.minimize = false;

  return config;
};
